﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sriasp.Models
{
    public class sriaspContext: DbContext
    {
        public sriaspContext(DbContextOptions<sriaspContext> options)
            : base(options)
        { }
        public DbSet<Contact> Contacts { get; set; }
    }
}
