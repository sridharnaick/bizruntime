﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace sriasp.Models
{
    public class Contact
    {
        [Required]
        public string id { get; set; }
        [MinLength(3)]
        public string Name { get; set; }
        [Required]
        public string age { get; set; }
    }
}
