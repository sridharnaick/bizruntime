﻿using System;
using System.Collections.Generic;

namespace sriasp.Entities
{
    public partial class Sri
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
    }
}
