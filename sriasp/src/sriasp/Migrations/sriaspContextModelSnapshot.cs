﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using sriasp.Models;

namespace sriasp.Migrations
{
    [DbContext(typeof(sriaspContext))]
    partial class sriaspContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("sriasp.Models.Contact", b =>
                {
                    b.Property<string>("id");

                    b.Property<string>("Name");

                    b.Property<string>("place");

                    b.HasKey("id");

                    b.ToTable("Contacts");
                });
        }
    }
}
