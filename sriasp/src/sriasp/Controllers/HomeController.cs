﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using sriasp.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace sriasp.Controllers
{
    public class HomeController : Controller
    {

        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            return View("IndexForm");
        }
        [HttpPost]
        
        public IActionResult Index(Contact contact)
        {
          //  if (ModelState.IsValid)
            {
                return View(contact);
            }
            // return LocalRedirect(RedirectUrl);
            //Contact contact = new Contact()
            //{
            //    id = id,
            //    Name = "sridhar", 
            //    place = "bangalore"
            //};

          //  return View("IndexForm");

        }
        public IActionResult DownloadData()
        {
            // return File("/DownloadData/AhaShare.txt", "text/plain", "ang.txt");
            return PhysicalFile("C:\\Users\\Sridhar\\Downloads\\Hachiko - A Dog's Tale (2009)\\AhaShare.com.txt", "text/plain");
        }
    }
}
